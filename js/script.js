window.onload = function() {
    document.getElementById('addFieldsMale').style.display = 'none';
    document.getElementById('addFieldsFemale').style.display = 'none';
}

function genderCheck() {
    if (document.getElementById('male').checked) {
        document.getElementById('addFieldsMale').style.display = 'block';
        document.getElementById('addFieldsFemale').style.display = 'none';
    }
    else if (document.getElementById('female').checked) {
        document.getElementById('addFieldsFemale').style.display = 'block';
        document.getElementById('addFieldsMale').style.display = 'none';
    }
}

function checkInfo() {
    var firstName = document.getElementById('firstName').value;
    var lastName = document.getElementById('lastName').value;
    var birthDate = document.getElementById('birthDate').value;
    var email = document.getElementById('email').value;

    if (firstName === "" || lastName === "" || birthDate === "" || email === "") {
        alert('Please fill in all fields');
        return false;
    }
    else {
        return true;
    }
}
